import React from "react";
import {Link} from 'react-router-dom';
const Nav=()=>{
    return(//redex
        <div>
           <ul className="nav-ul">
            <li><Link to="/">intro</Link></li>
            <li><Link to="/add">Add Questions</Link></li>
            <li><Link to="/update">Update Question</Link></li>
            <li><Link to="/logout">logout</Link></li>
            <li><Link to="/signup">Sign Up</Link></li>
           </ul>
        </div>
    )
}

export default Nav;