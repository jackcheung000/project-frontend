
import './App.css';
import Nav from'./Nav';
import {BrowserRouter, Routes, Route } from "react-router-dom"//引入路由
import SignUp from './Signup';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Nav />
      
      <Routes>
        <Route path='/'element={<h1>intro</h1>}/>
        <Route path='/add'element={<h1>Add Question</h1>}/>
        <Route path='/update'element={<h1>Update Question</h1>}/>
        <Route path='/logout'element={<h1>logout</h1>}/>
        <Route path='/signup'element={<SignUp/>}/>

      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
