import React,{useState} from "react";

const SignUp=()=>{
    const[name,setName]=useState("");
    const[email,setEmail]=useState("");
    const[password,setPassword]=useState("");
    const collectData=async ()=>{
        console.log(name,email,password);
        let data = await fetch("/register",{
            method:'POST',
            body:JSON.stringify({name,email,password}),
            headers:{
                'Content-Type':'application/json'
            },
        });
        data = await data.json()
        console.log(data);
    }
    return(
        <div className="register">
            <h1>Register</h1>
            <input className="inputBox" type="text" placeholder="Enter Name"
            value={name} onChange={(e)=>setName(e.target.value)}/>
            <input className="inputBox" type="text" placeholder="Enter Email"
            value={email} onChange={(e)=>setEmail(e.target.value)}/>
            <input className="inputBox" type="text" placeholder="Enter Password"
            value={password} onChange={(e)=>setPassword(e.target.value)}/>
            <button onClick={collectData} className="appButton" type="button">Sign Up</button>



        </div>
    )
}

export default SignUp